package com.mycompany.telas;

import java.io.IOException;

import javafx.fxml.FXML;
import personagens.Alex;
import personagens.Capitao;
import personagens.Gloria;
import personagens.Julian;
import personagens.Kowalski;
import personagens.Marty;
import personagens.Melman;
import personagens.Mort;
import personagens.Recruta;
import personagens.Personagem;
import personagens.Rico;


public class PrimaryController{
  
    @FXML
    private void switchToSecondary() throws IOException{
        App.setRoot("telaPersonagem");
    }
    
    public void setPersonagemRecruta() throws IOException {
       
        Personagem recruta = new Recruta();
        App.setPersonagem(recruta);
        switchToSecondary();
    }

    public void setPersonagemCapitao() throws IOException {
       
        Personagem capitao = new Capitao();
        App.setPersonagem(capitao);
        switchToSecondary();
    }
     
    public void setPersonagemRico() throws IOException {
       
        Personagem rico = new Rico();
        App.setPersonagem(rico);
        switchToSecondary();
    }
    
    public void setPersonagemKowalski() throws IOException {
       
        Personagem kowalski = new Kowalski();
        App.setPersonagem(kowalski);
        switchToSecondary();
    }
     
    public void setPersonagemGloria() throws IOException {
       
        Personagem gloria = new Gloria();
        App.setPersonagem(gloria);
        switchToSecondary();
    }
     
    public void setPersonagemJulian() throws IOException {
       
        Personagem julian = new Julian();
        App.setPersonagem(julian);
        switchToSecondary();
    }
     
    public void setPersonagemMort() throws IOException {
       
        Personagem mort = new Mort();
        App.setPersonagem(mort);
        switchToSecondary();
    }
    
    public void setPersonagemMarty() throws IOException {
       
        Personagem marty = new Marty();
        App.setPersonagem(marty);
        switchToSecondary();
    }
     
    public void setPersonagemAlex() throws IOException {
       
        Personagem alex = new Alex();
        App.setPersonagem(alex);
        switchToSecondary();
    }
     
    public void setPersonagemMelman() throws IOException {
       
        Personagem melman = new Melman();
        App.setPersonagem(melman);
        switchToSecondary();
    }
    
    


   
    
 
}
        
    
