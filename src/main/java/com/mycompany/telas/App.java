package com.mycompany.telas;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import personagens.Personagem;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static Personagem personagem;
    
    public static void setPersonagem(Personagem p){
        App.personagem = p;
    }
    
     public static Personagem getPersonagem(){
        return App.personagem;
    }
    
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("telaInicial"), 1000, 700);
        stage.setScene(scene);
        stage.show();
    }
    
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}