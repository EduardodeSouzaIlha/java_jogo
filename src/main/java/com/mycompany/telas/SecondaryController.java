package com.mycompany.telas;

import java.net.URL;
import javafx.fxml.FXML;

import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import personagens.Personagem;
import javafx.scene.image.ImageView;


public class SecondaryController implements Initializable {
    @FXML
    private Label cansaco;

    @FXML
    private Label dinheiro;

    @FXML
    private Label diversao;

    @FXML
    private Label fome;

    @FXML
    private Label higiene;
    
    @FXML
    private Label nome;
    
    private Personagem personagem;
   
    @FXML
    private ImageView image;
    
   


   
 
    
    public void trabalhar(ActionEvent event){
        personagem.trabalhar();
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));

    }
   public void divertir(ActionEvent event){
        personagem.divertir();
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));
   }
    public void comer(ActionEvent event){
        personagem.comer();
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));
   }
    public void banho(ActionEvent event){
        personagem.banho();
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));
    }
    public void dormir(ActionEvent event){
        personagem.dormir();
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));
    }
 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        personagem = App.getPersonagem();
        nome.setText(personagem.getNome());
        dinheiro.setText(Integer.toString(personagem.getDinheiro()));
        diversao.setText(Integer.toString(personagem.getDiversao()));
        fome.setText(Integer.toString(personagem.getFome()));
        higiene.setText(Integer.toString(personagem.getHigiene()));
        cansaco.setText(Integer.toString(personagem.getEnergia()));
    }    
}