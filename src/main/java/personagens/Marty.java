/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package personagens;

/**
 *
 * @author 02150179
 */
public class Marty extends Personagem{
     public Marty(){
            this.setNome("Marty");
            this.setEnergia(100);
            this.setDinheiro(0);
            this.setHigiene(100);
            this.setFome(0);
            this.setDiversao(100);
        }
        
        
     @Override
        public void trabalhar(){
            this.setDinheiro(this.getDinheiro()+1);
        }
    
     @Override
        public void divertir(){
           this.setDiversao(this.getDiversao()+1);
        }
    
     @Override
        public void banho(){
             this.setHigiene(this.getHigiene()+1);
        }
    
     @Override
        public void dormir(){
             this.setEnergia(this.getEnergia()+1);
        }
    
     @Override
        public void comer(){
             this.setFome(this.getFome()+1);
        }
}
