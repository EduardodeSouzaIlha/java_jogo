/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package personagens;

/**
 *
 * @author 02150179
 */
public class Alex extends Personagem{
     public Alex(){
            this.setNome("Alex");
            this.setEnergia(50);
            this.setDinheiro(0);
            this.setHigiene(100);
            this.setFome(0);
            this.setDiversao(100);
        }
        
        
     @Override
        public void trabalhar(){
            this.setDinheiro(this.getDinheiro()+20);
            this.setHigiene(this.getHigiene()-10);
            this.setFome(this.getFome()+10);
        }
    
     @Override
        public void divertir(){
           this.setDiversao(this.getDiversao()+10);
           this.setHigiene(this.getHigiene()-5);
           this.setFome(this.getFome()+2);
        }
    
     @Override
        public void banho(){
             this.setHigiene(this.getHigiene()+15);
             
        }
    
     @Override
        public void dormir(){
             this.setEnergia(this.getEnergia()+10);
             this.setFome(this.getFome()+2);
        }
    
     @Override
        public void comer(){
             this.setFome(this.getFome()-5);
              this.setEnergia(this.getEnergia()+5);
             
        }
}
