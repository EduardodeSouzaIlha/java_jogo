/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package personagens;

/**
 *
 * @author 02150179
 */
public abstract class Personagem{
    private String nome;
    private int energia;
    private int dinheiro;
    private int fome;
    private int higiene;
    private int diversao;
    
    public String getNome() {
        return nome;
    }

    public int getEnergia() {
        return energia;
    }

    public void setEnergia(int cansaco) {
        if(energia <= 100 && energia >= 0){
            this.energia = cansaco;
        }
    }

    public int getFome() {
        return fome;
    }

    public void setFome(int fome) {
        if(fome <= 100 && fome >= 0){
            this.fome = fome;
        }
    }

    public int getHigiene() {
        return higiene;
    }

    public void setHigiene(int higiene) {
        if(higiene <= 100 && higiene >= 0){
            this.higiene = higiene;
        }
    }

    public int getDiversao() {
        return diversao;
    }

    public void setDiversao(int diversao) {
        if(diversao <= 100 && diversao >= 0 ){
            this.diversao = diversao;
        }
       
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(int dinheiro) {
        if(dinheiro >= 0){
                this.dinheiro = dinheiro;
        }
    }
    public abstract void trabalhar();
    
    public abstract void divertir();
    
    public abstract void banho();
    
    public abstract void dormir();
    
    public abstract void comer();
    
    
    
}
